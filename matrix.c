#include "matrix.h"
#include <stdio.h>
#include <stdlib.h>

matrix* create_matrix(int width, int height) {
  matrix* m = malloc(sizeof(matrix));
  m->width = width;
  m->height = height;
  m->data = malloc(width * height * sizeof(int));
  return m;
}

matrix* create_zero_matrix(int width, int height) {
  matrix* m = malloc(sizeof(matrix));
  m->width = width;
  m->height = height;
  m->data = calloc(width * height, sizeof(int));
  return m;
}

void free_matrix(matrix* m) {
  free(m->data);
  free(m);
}

int get(matrix* m, int x, int y) {
  return m->data[y * m->height + x];
}

int set(matrix* m, int x, int y, int value) {
  return m->data[y * m->height + x] = value;
}

matrix* add(matrix* m1, matrix* m2) {
  if (m1->height != m2->height || m1->width != m2->width) {
    fprintf(stderr, "matrix dimensions differ\n");
    return NULL;
  }
  matrix* m = create_matrix(m1->width, m1->height);
  for (int i = 0; i < m->width * m->height; i++)
    m->data[i] = m1->data[i] + m2->data[i];

  return m;
}

matrix* scale(matrix* m, int s) {
  matrix* sm = create_matrix(m->width, m->height);
  for (int i = 0; i < m->width * m->height; i++)
    sm->data[i] = s * m->data[i];
  return sm;
}

int matrix_min(matrix* m) {
  if (!m->height || !m->width) return 0;
  int min = get(m, 0, 0);
  for (int i = 0; i < m->width * m->height; i++)
    if (m->data[i] < min) min = m->data[i];
  return min;
}

int matrix_max(matrix* m) {
  if (!m->height || !m->width) return 0;
  int max = get(m, 0, 0);
  for (int i = 0; i < m->width * m->height; i++)
    if (m->data[i] > max) max = m->data[i];
  return max;
}

void print_matrix(matrix* m) {
  printf("[ ");
  int start = 2;
  for (int j = 0; j < m->height; j++) {
    if (start) start--;
    else printf("\n");
    for (int i = 0; i < m->width; i++) {
      if (start) start--;
      else printf(", ");
      printf("% 4d", get(m, i, j));
    }
  }
  printf("    ]\n");
}

matrix* scale(matrix* m, int factor) {
  matrix* result = create_matrix(m->width, m->height);
  for (int i = 0; i < m->width * m->height; i++)
    m->data[i] *= factor;
  return result;
}
