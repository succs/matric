#include <stdio.h>
#include "matrix.h"

int main(void) {
  printf("Hello world!\n");

  printf("Creating a matrix\n");
  matrix* m = create_zero_matrix(2, 3);
  //set(m, 0, 2, 42);
  print_matrix(m);

  printf("Freeing the matrix\n");
  free_matrix(m);

  printf("\x1b[32mOK\x1b[0m (·_· )\n");
  printf("Noice\n");
  return 0;
}
