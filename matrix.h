#ifndef __MATRIX__
#define __MATRIX__

typedef struct matrix {
  int width;
  int height;
  int* data;
} matrix;

matrix* create_matrix(int width, int height);
matrix* create_zero_matrix(int width, int height);
void free_matrix(matrix* m);
int get(matrix* m, int x, int y);
int set(matrix* m, int x, int y, int value);
matrix* add(matrix* m1, matrix* m2);
void print_matrix(matrix* m);
matrix* scale(matrix* m, int s);
int matrix_min(matrix* m);

#endif
